%% Rectification parameters
% When present, these parameters override the defaults in:
% - coRectify.m
% - coGeoRectify.m

%%
%% James River Deployment March 2019
%%
%%
%% De-jittering (coRectify and coGeoRectify) parameters
doOverwrite     = true;             % if True, overwrites the "data" field
                                    % if False, adds "dataCoRect" field
refCoords       = [120 155 1/20     1850 inf 100]; % In degrees and meters
searchWidth     = 15;               % Search region width beyond edge of reference feature (in degrees)
interpMethod    = 'spline';         % For griddedInterpolant.m ... 'linear','spline','nearest', etc.
threshLag       = searchWidth;      % Frames with lags beyond this value don't get adjusted (in degrees)
controlCube     = 'JsRiverN_refCube.mat';% Name of reference Cube. Must have correct values of:
                                    % Rg, Azi, timex, heading, XOrigin, YOrigin, UTMZone